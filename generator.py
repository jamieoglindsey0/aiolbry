"""Generates the python bindings based on the JSON API."""

import aiohttp
import aiofiles
import asyncio
from json import loads
from aiolbry.constants import (
    LBRY_API_RAW_JSON_URL,
    DTYPE_MAPPING,
    LBRYD_FPATH,
    __LBRYD_BASE_FPATH__,
)


async def get_lbry_api_function_docs(url=LBRY_API_RAW_JSON_URL):
    """
    Scrapes the given URL to a page in JSON format.

    We do this to obtain the documentation for the LBRY API.

    :param str url: URL to the documentation we need to obtain,
        aiolbry.constants.LBRY_API_RAW_JSON_URL by default.
    :return: List of functions retrieved from the `url` given.
    :rtype: [(list)]
    """
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as resp:
                return loads(await resp.text())

    except Exception as error:
        print(error)

    return []


def generate_method_definition(func):
    """Generates the body for the given function.

    :param dict func: dict of a JSON-Formatted function as defined by
        the API docs
    :return: A String containing the definition for the function as it
        should be written in code
    :rtype: str
    """
    print(f"[DEBUG]:: func: {func}")
    # func = func["commands"]
    # print(f"[DEBUG]:: func['commands']: {func}")
    indent = 4

    # initial definition
    method_definition = f"{' ' * indent}async def {func['name']}"

    # Here we just create a queue and put all the parameters
    # into the queue in the order that they were given,
    params_required = [
        param for param in func["arguments"] if param["is_required"]
    ]
    params_optional = [
        param for param in func["arguments"] if not param["is_required"]
    ]

    # Open the parameter definitions
    method_definition += "(self, "

    for param in params_required:
        # Put the parameter into the queue
        if param["name"] == "blobs_in_stream<blobs_in_stream>":
            param["name"] = param["name"].replace(
                "blobs_in_stream<blobs_in_stream>", "blobs_in_stream"
            )

        method_definition += f"{param['name']}, "

    for param in params_optional:
        if param["name"] == "blobs_in_stream<blobs_in_stream>":
            param["name"] = param["name"].replace(
                "blobs_in_stream<blobs_in_stream>", "blobs_in_stream"
            )
        # Default methods not required
        method_definition += (
            f"{param['name']}=None, "
            # TODO: FIXME: make this able to parse `str, list` etc. into
            # Optional[Union[str, list]]
            # f"{param['name']}: {DTYPE_MAPPING[param['type'].lower()]} = None, "
        )

    # Peel off the final ", " and close off the parameter definition
    method_definition = f"{method_definition.rstrip(', ')}):\n"

    indent += 4

    # re-indent
    method_definition += " " * indent

    # Begin with description.

    method_definition += f'"""{func["description"]}'

    # re-indent
    method_definition += f"\n\n{' ' * indent}"

    # Go through each parameter and insert description & type hint
    for param in params_required + params_optional:
        # Add the type
        method_definition += (
            f":param [({DTYPE_MAPPING[param['type'].lower()]})]"
        )

        # Add the name
        method_definition += f" {param['name']}: {param['description']}"

        # Add optionality & reindent
        method_definition += "\n" if param["is_required"] else " (Optional)\n"

        method_definition += " " * indent

    open_index = func["returns"].find("[(")
    close_index = func["returns"].find(
        ")]", (open_index if open_index > -1 else 0)
    )
    _tab = "\t"
    func["returns"] = f"{func['returns'].replace(_tab, ' ' * 4)}"
    return_string = func["returns"]  # .replace("\n", "")

    if (
        open_index < close_index
        and func["returns"][open_index + 1 : close_index] in DTYPE_MAPPING
    ):
        method_definition += (
            ":rtype: "
            + DTYPE_MAPPING[func["returns"][open_index + 1 : close_index]]
        )

        func["returns"] = func["returns"].replace(
            func["returns"][open_index : close_index + 1], ""
        )

        method_definition += "\n"

    method_definition += (
        f":return:\n{' ' * indent}{return_string}\n{' ' * indent}"
    )

    # Close it off & reindent
    method_definition += f'"""\n{" " * indent}'

    # Create the params map
    params_map = "__params_map = {"

    # Save the indent
    params_indent, num_params = len(params_map), len(params_required) + len(
        params_optional
    )

    # Append the map to the method_definition
    method_definition += params_map

    # Go through the required parameters first
    for i, param in enumerate(params_required + params_optional):

        # append the methods to the map
        if param["name"] == "blobs_in_stream<blobs_in_stream>":
            param["name"] = param["name"].replace(
                "blobs_in_stream<blobs_in_stream>", "blobs_in_stream"
            )
        method_definition += f"'{param['name']}': {param['name']}"

        if not param["is_required"]:
            method_definition + f" if {param['name']} is not None else None"

        # add commas or ending bracket if needed & reindent correctly
        method_definition += (
            f",\n{' ' * indent}{' ' * params_indent}"
            if i + 1 < num_params
            else ""
        )

    method_definition += f"}}\n\n{' ' * indent}"

    method_definition += (
        f"request = await self.make_request(SERVER_ADDRESS, '{func['name']}"
        f"', {params_map.rstrip(' = {')}"
        f")\n{' ' * indent}return request\n\n"
    )

    return method_definition


# Currently this only supports LBRYD, as LBRYCRD's API is nowhere to be found,
# Therefore anyone wanting to use that needs to call the functions manually.
async def _generate_lbryd_wrapper(
    url=LBRY_API_RAW_JSON_URL,
    read_file=__LBRYD_BASE_FPATH__,
    write_file=LBRYD_FPATH,
):
    """
    Generates the actual functions for lbryd_api.py using lbry's documentation.

    :param str url: URL to the documentation we need to obtain,
        aiolbry.constants.LBRY_API_RAW_JSON_URL by default.
    :param str read_file: This is the path to the file from which
        we will be reading.
    :param str write_file: Path from project root to the file we'll
        be writing to.
    """

    functions = await get_lbry_api_function_docs(url)
    # logger.debug(f"{functions[:100]}")

    # Open the actual file for appending
    async with aiofiles.open(write_file, "w") as lbry_file:

        await lbry_file.write(
            "# This file was generated from the official LBRY docs.\n"
        )
        await lbry_file.write("# You may edit but do so with caution.\n")

        async with aiofiles.open(read_file, "r") as template:
            header = await template.read()

        await lbry_file.write(header)

        # Iterate through all the functions we retrieved
        for function in functions:
            for command in functions[function]["commands"]:

                method_definition = generate_method_definition(command)

                # Write to file
                await lbry_file.write(method_definition)

    try:
        from black import FileMode, format_file_contents

        async with aiofiles.open(LBRYD_FPATH, "r") as f:
            code = await f.read()
            out = format_file_contents(code, fast=False, mode=FileMode())
        async with aiofiles.open(LBRYD_FPATH, "w") as f:
            await f.write(out)

    except ImportError as error:
        print("[Warning]: black is not installed, cannot format document.")
        print(error)


def generate_lbryd_wrapper():
    asyncio.run(_generate_lbryd_wrapper())
