from typing import Optional, Dict


def print_request(request) -> None:
    """
    Print a prepared request to give the user info as to what they're sending.

    :param [(aiohttp.ClientRequest)] request: ClientRequest
        object to be printed.
    :return: None
    """
    _nl = "\n"
    print(
        f"-----------START-----------\n"
        f"{request.method} {request.url}\n"
        f"{_nl.join(k + ': ' + v for k, v in request.headers.items())}\n\n"
        f"{request.body}"
    )


class LBRYException(Exception):
    def __init__(
        self,
        message: Optional[str],
        response: Optional[Dict],
        status_code: Optional[int],
        # request: aiohttp.ClientRequest,
    ):
        """

        :param str message: Message to display.
        :param dict response: JSON Response received from LBRY.
        :param int status_code: HTTP Status code received from HTTP request.
        :param aiohttp.ClientRequest request: ClientRequest object which
            raised the exception
        """

        # Call the
        super().__init__(message)

        self.response = response
        self.status_code = status_code
        # self.request = request
