"""
What needs to be done is the following.

An API Wrapper for LBRY.io needs to be made in order to
make requests to the API.

The only two examples I've seen both were making requests
to the authorized-api, which required a username and a password.
Since this is sort of confusing and not very test-friendly if
you want to make requests without having a username and password.


Basic idea is as follows: we need to make a POST request to a given URL,
with a 'method' (api function), and the parameters we give it.

We want to be able to continuously make requests if we need to,
and so we will encapsulate this as a class.

"""

import aiohttp
from typing import Optional, Union, Dict
from aiolbry.exceptions import LBRYException


class BaseApi(object):
    """The BaseAPI class for other API classes."""

    def __init__(self, request_id: int = 0):
        """Initialize BaseApi.

        :param float timeout: The number of seconds to wait for a
        connection until we time out.
        """
        self.session = aiohttp.ClientSession()

        self.request_id = request_id

    async def make_request(
        self,
        url: str,
        method: str,
        params: Optional[Dict] = None,
        basic_auth: Optional[Union[list, tuple]] = None,
    ) -> Optional[tuple]:
        """
        Make a POST request to the given URL.

        Specifying the data to be passed in as:
            {"method": method, "params": parameters}

        :param [(str)] url: URL to connect to.
        :param [(str)] method: The API method to call.
        :param [(dict)] params: Dictionary object of the parameters
            associated with the `method` given. None by default.
        :param [(list)] | [(tuple)] basic_auth: List containing your
            username and password as ['username', 'password'].
            This is empty by default, however it is required by all of
            the `lbrycrd` methods
        :raises LBRYException: If the request returns an error when
            calling the API
        :return: A `dict` of the JSON result member of the request
        :rtype: dict
        """
        params = {} if params is None else params
        self.request_id += 1
        params = {k: v for (k, v) in params.items() if v is not None}
        data = {
            "method": method,
            "params": params,
            "jsonrpc": "2.0",
            "id": self.request_id,
        }
        headers = {
            "Content-Type": "application/json-rpc",
            "user-agent": "LBRY python3-api",
        }
        timeout = aiohttp.ClientTimeout()
        response = await self.session.post(
            url,
            json=data,
            headers=headers,
            auth=basic_auth,
            timeout=timeout,
        )
        response_json = await response.json()
        if "result" in response_json:
            return (response_json["result"], response)
        elif "error" in response_json:
            raise LBRYException(
                "POST Request made to LBRY received an error",
                response_json,
                response.status,
            )
        else:
            return (None, None)
        # timeout = aiohttp.ClientTimeout(total=60)
        # async with self.session.post(
        #     url,
        #     json=data,
        #     headers=headers,
        #     auth=basic_auth,
        #     # timeout=timeout,
        # ) as response:
        #     response_json = await response.json()
        #     if "result" in response_json:
        #         return (response_json["result"], response)
        #     elif "error" in response_json:
        #         raise LBRYException(
        #             "POST Request made to LBRY received an error",
        #             response_json,
        #             response.status,
        #         )
