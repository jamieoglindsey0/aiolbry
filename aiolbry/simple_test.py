import asyncio
from aiolbry.lbryd_api import LbrydApi

lbry = LbrydApi()
loop = asyncio.get_event_loop()
loop.run_until_complete(lbry.ffmpeg_find())