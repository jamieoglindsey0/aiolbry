"""Defines server constants so they dont get altered by other files."""

LBRYD_SERVER_ADDRESS = "http://localhost:5279"
LBRYCRD_SERVER_ADDRESS = "http://localhost:9245"

LBRY_API_RAW_JSON_URL = (
    "https://raw.githubusercontent.com/lbryio/lbry/master/docs/api.json"
)

DTYPE_MAPPING = {
    "list": "list",
    "decimal": "float",
    "float": "float",
    "bool": "bool",
    "int": "int",
    "dict": "dict",
    "string": "str",
    "str": "str",
    "str, list": "str, list",
    "str or list": "str, list",
    "date": "datetime",
}

# LBRYCRD documentation doesn't exist at least that I could find
# LBRYCRD_API_RAW_JSON_URL = ""


# This is the file that is used in pre-generation and should NOT be overwritten
__LBRYD_BASE_FPATH__ = "aiolbry/__lbryd_api__.py"

LBRYD_FPATH = "aiolbry/lbryd_api.py"
