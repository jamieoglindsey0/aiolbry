# This file was generated from the official LBRY docs.
# You may edit but do so with caution.
from typing import Optional, Dict
from aiolbry.base_api import BaseApi
from aiolbry.constants import LBRYD_SERVER_ADDRESS as SERVER_ADDRESS


class LbrydApi(BaseApi):
    async def call(
        self, method, params: Optional[Dict] = None
    ): 
        """
        Make a Call to the LBRY API.

        :param [(str)] method: Method to call from the LBRY API.
            See the full list of methods at
            https://lbryio.github.io/lbry/cli/
        :param [(dict)] params: Parameters to give the method selected
        :param float timeout: The number of seconds to wait for a connection
            until we time out; 600 By Default.
        :raises LBRYException: If the request returns an error when calling
            the API
        :return: A Python `dict` object containing the data requested
            from the API
        :rtype: [(dict)]
        """

        params = {} if params is None else params

        request = await self.make_request(SERVER_ADDRESS, method, params)
        return request

    async def ffmpeg_find(self):
        """Get ffmpeg installation information

                :return:
        (dict) Dictionary of ffmpeg information
            {
                'available': (bool) found ffmpeg,
                'which': (str) path to ffmpeg,
                'analyze_audio_volume': (bool) should ffmpeg analyze audio
            }
        """
        __params_map = {}

        request = await self.make_request(
            SERVER_ADDRESS, "ffmpeg_find", __params_map
        )
        return request

    async def get(
        self,
        uri=None,
        file_name=None,
        download_directory=None,
        timeout=None,
        save_file=None,
        wallet_id=None,
    ):
        """Download stream from a LBRY name.

        :param [(str)] uri: uri of the content to download (Optional)
        :param [(str)] file_name: specified name for the downloaded file,
            overrides the stream file name (Optional)
        :param [(str)] download_directory: full path to the directory to
            download into (Optional)
        :param [(int)] timeout: download timeout in number of
            seconds (Optional)
        :param [(bool)] save_file: save the file to the downloads directory
            (Optional)
        :param [(str)] wallet_id: wallet to check for claim purchase
             reciepts (Optional)
        :return:
            {
                "streaming_url": "(str) url to stream the file using
                    range requests",
                "completed": "(bool) true if download is completed",
                "file_name": "(str) name of file",
                "download_directory": "(str) download directory",
                "points_paid": "(float) credit paid to download file",
                "stopped": "(bool) true if download is stopped",
                "stream_hash": "(str) stream hash of file",
                "stream_name": "(str) stream name",
                "suggested_file_name": "(str) suggested file name",
                "sd_hash": "(str) sd hash of file",
                "download_path": "(str) download path of file",
                "mime_type": "(str) mime type of file",
                "key": "(str) key attached to file",
                "total_bytes_lower_bound": "(int) lower bound file size in
                    bytes",
                "total_bytes": "(int) file upper bound size in bytes",
                "written_bytes": "(int) written size in bytes",
                "blobs_completed": "(int) number of fully downloaded blobs",
                "blobs_in_stream": "(int) total blobs on stream",
                "blobs_remaining": "(int) total blobs remaining to download",
                "status": "(str) downloader status",
                "claim_id": "(str) None if claim is not found else the claim
                    id",
                "txid": "(str) None if claim is not found else the transaction
                    id",
                "nout": "(int) None if claim is not found else the transaction
                    output index",
                "outpoint": "(str) None if claim is not found else the tx and
                    output",
                "metadata": "(dict) None if claim is not found else the claim
                    metadata",
                "channel_claim_id": "(str) None if claim is not found or not
                    signed",
                "channel_name": "(str) None if claim is not found or not
                    signed",
                "claim_name": "(str) None if claim is not found else the claim
                    name",
                "reflector_progress": "(int) reflector upload progress, 0 to
                    100",
                "uploading_to_reflector": "(bool) set to True when currently
                    uploading to reflector"
            }
        """
        __params_map = {
            "uri": uri,
            "file_name": file_name,
            "download_directory": download_directory,
            "timeout": timeout,
            "save_file": save_file,
            "wallet_id": wallet_id,
        }

        request = await self.make_request(SERVER_ADDRESS, "get", __params_map)
        return request

    async def publish(
        self,
        name,
        bid=None,
        file_path=None,
        validate_file=None,
        optimize_file=None,
        fee_currency=None,
        fee_amount=None,
        fee_address=None,
        title=None,
        description=None,
        author=None,
        tags=None,
        languages=None,
        locations=None,
        license=None,
        license_url=None,
        thumbnail_url=None,
        release_time=None,
        width=None,
        height=None,
        duration=None,
        channel_id=None,
        channel_name=None,
        channel_account_id=None,
        account_id=None,
        wallet_id=None,
        funding_account_ids=None,
        claim_address=None,
        preview=None,
        blocking=None,
    ):
        """Create or replace a stream claim at a given name (use 'stream
            create/update' for more control).

        :param [(str)] name: name of the content (can only consist
            of a-z A-Z 0-9 and -(dash))
        :param [(float)] bid: amount to back the claim (Optional)
        :param [(str)] file_path: path to file to be associated with
             name. (Optional)
        :param [(bool)] validate_file: validate that the video container
            and encodings match common web browser support or that
            optimization succeeds if specified. FFmpeg is required (Optional)
        :param [(bool)] optimize_file: transcode the video & audio if
            necessary to ensure common web browser support.
            FFmpeg is required (Optional)
        :param [(str)] fee_currency: specify fee currency (Optional)
        :param [(float)] fee_amount: content download fee (Optional)
        :param [(str)] fee_address: address where to send fee payments,
            will use value from --claim_address if not provided (Optional)
        :param [(str)] title: title of the publication (Optional)
        :param [(str)] description: description of the publication (Optional)
        :param [(str)] author: author of the publication. The usage for this
            field is not the same as for channels. The author field is used
            to credit an author who is not the publisher and is not represented
            by the channel. For example, a pdf file of 'The Odyssey' has an
            author of 'Homer' but may by published to a channel such as
            '@classics', or to no channel at all (Optional)
        :param [(list)] tags: add content tags (Optional)
        :param [(list)] languages: languages used by the channel, using
            RFC 5646 format, eg: for English `--languages=en` for
                Spanish (Spain) `--languages=es-ES` for Spanish (Mexican)
                `--languages=es-MX` for Chinese (Simplified)
                `--languages=zh-Hans` for Chinese (Traditional)
                `--languages=zh-Hant` (Optional)
        :param [(list)] locations: locations relevant to the stream,
            consisting of 2 letter `country` code and a `state`, `city` and
            a postal `code` along with a `latitude` and `longitude`.
            for JSON RPC: pass a dictionary with aforementioned attributes as
            keys, eg: ... "locations": [{'country': 'US', 'state': 'NH'}] ...
            for command line: pass a colon delimited list with values in
            the following order: "COUNTRY:STATE:CITY:CODE:LATITUDE:LONGITUDE"
            making sure to include colon for blank values, for example to
            provide only the city: ... --locations="::Manchester" with all
            values set: ...
            --locations="US:NH:Manchester:03101:42.990605:-71.460989"
            optionally, you can just pass the "LATITUDE:LONGITUDE": ...
             --locations="42.990605:-71.460989" finally, you can also pass
             JSON string of dictionary on the command line as you would via
             JSON RPC ... --locations="{'country': 'US', 'state': 'NH'}"
             (Optional)
        :param [(str)] license: publication license (Optional)
        :param [(str)] license_url: publication license url (Optional)
        :param [(str)] thumbnail_url: thumbnail url (Optional)
        :param [(int)] release_time: original public release of content,
            seconds since UNIX epoch (Optional)
        :param [(int)] width: image/video width, automatically calculated from
            media file (Optional)
        :param [(int)] height: image/video height, automatically calculated
            from media file (Optional)
        :param [(int)] duration: audio/video duration in seconds,
            automatically calculated (Optional)
        :param [(str)] channel_id: claim id of the publisher channel (Optional)
        :param [(str)] channel_name: name of publisher channel (Optional)
        :param [(str)] channel_account_id: one or more account ids for
            accounts to look in for channel certificates, defaults to all
            accounts. (Optional)
        :param [(str)] account_id: account to use for holding the transaction
            (Optional)
        :param [(str)] wallet_id: restrict operation to specific wallet
            (Optional)
        :param [(list)] funding_account_ids: ids of accounts to fund this
            transaction (Optional)
        :param [(str)] claim_address: address where the claim is sent to,
            if not specified it will be determined automatically from the
            account (Optional)
        :param [(bool)] preview: do not broadcast the transaction (Optional)
        :param [(bool)] blocking: wait until transaction is in mempool (
                Optional)
        :return:
            {
                "txid": "hash of transaction in hex",
                "height": "block where transaction was recorded",
                "inputs": [
                    {
                        "txid": "hash of transaction in hex",
                        "nout": "position in the transaction",
                        "height": "block where transaction was recorded",
                        "amount": "value of the txo as a decimal",
                        "address": "address of who can spend the txo",
                        "confirmations": "number of confirmed blocks",
                        "is_change": "payment to change address, only
                            available when it can be determined",
                        "is_received": "true if txo was sent from external
                            account to this account",
                        "is_spent": "true if txo is spent",
                        "is_mine": "payment to one of your accounts, only
                            available when it can be determined",
                        "type": "one of 'claim', 'support' or 'purchase'",
                        "name": "when type is 'claim' or 'support', this
                            is the claim name",
                        "claim_id": "when type is 'claim', 'support' or
                            'purchase', this is the claim id",
                        "claim_op": "when type is 'claim', this determines if
                            it is 'create' or 'update'",
                        "value": "when type is 'claim' or 'support' with
                            payload, this is the decoded protobuf payload",
                        "value_type": "determines the type of the 'value'
                            field: 'channel', 'stream', etc",
                        "protobuf": "hex encoded raw protobuf version of
                            'value' field",
                        "permanent_url": "when type is 'claim' or 'support',
                            this is the long permanent claim URL",
                        "claim": "for purchase outputs only, metadata of
                            purchased claim",
                        "reposted_claim": "for repost claims only, metadata of
                            claim being reposted",
                        "signing_channel": "for signed claims only, metadata
                            of signing channel",
                        "is_channel_signature_valid": "for signed claims only,
                            whether signature is valid",
                        "purchase_receipt": "metadata for the purchase
                            transaction associated with this claim"
                    }
                ],
                "outputs": [
                    {
                        "txid": "hash of transaction in hex",
                        "nout": "position in the transaction",
                        "height": "block where transaction was recorded",
                        "amount": "value of the txo as a decimal",
                        "address": "address of who can spend the txo",
                        "confirmations": "number of confirmed blocks",
                        "is_change": "payment to change address, only
                            available when it can be determined",
                        "is_received": "true if txo was sent from external
                            account to this account",
                        "is_spent": "true if txo is spent",
                        "is_mine": "payment to one of your accounts, only
                            available when it can be determined",
                        "type": "one of 'claim', 'support' or 'purchase'",
                        "name": "when type is 'claim' or 'support', this is
                            the claim name",
                        "claim_id": "when type is 'claim', 'support' or
                            'purchase', this is the claim id",
                        "claim_op": "when type is 'claim', this determines if
                            it is 'create' or 'update'",
                        "value": "when type is 'claim' or 'support' with
                            payload, this is the decoded protobuf payload",
                        "value_type": "determines the type of the 'value'
                            field: 'channel', 'stream', etc",
                        "protobuf": "hex encoded raw protobuf version of
                            'value' field",
                        "permanent_url": "when type is 'claim' or 'support',
                            this is the long permanent claim URL",
                        "claim": "for purchase outputs only, metadata of
                            purchased claim",
                        "reposted_claim": "for repost claims only, metadata of
                            claim being reposted",
                        "signing_channel": "for signed claims only, metadata
                            of signing channel",
                        "is_channel_signature_valid": "for signed claims only,
                            whether signature is valid",
                        "purchase_receipt": "metadata for the purchase
                            transaction associated with this claim"
                    }
                ],
                "total_input": "sum of inputs as a decimal",
                "total_output": "sum of outputs, sans fee, as a decimal",
                "total_fee": "fee amount",
                "hex": "entire transaction encoded in hex"
            }
        """
        __params_map = {
            "name": name,
            "bid": bid,
            "file_path": file_path,
            "validate_file": validate_file,
            "optimize_file": optimize_file,
            "fee_currency": fee_currency,
            "fee_amount": fee_amount,
            "fee_address": fee_address,
            "title": title,
            "description": description,
            "author": author,
            "tags": tags,
            "languages": languages,
            "locations": locations,
            "license": license,
            "license_url": license_url,
            "thumbnail_url": thumbnail_url,
            "release_time": release_time,
            "width": width,
            "height": height,
            "duration": duration,
            "channel_id": channel_id,
            "channel_name": channel_name,
            "channel_account_id": channel_account_id,
            "account_id": account_id,
            "wallet_id": wallet_id,
            "funding_account_ids": funding_account_ids,
            "claim_address": claim_address,
            "preview": preview,
            "blocking": blocking,
        }

        request = await self.make_request(
            SERVER_ADDRESS, "publish", __params_map
        )
        return request

    async def resolve(
        self,
        urls=None,
        wallet_id=None,
        new_sdk_server=None,
        include_purchase_receipt=None,
        include_is_my_output=None,
        include_sent_supports=None,
        include_sent_tips=None,
        include_received_tips=None,
    ):
        """Get the claim that a URL refers to.

                :param [(str, list)] urls: one or more urls to resolve
                    (Optional)
                :param [(str)] wallet_id: wallet to check for claim purchase
                    reciepts (Optional)
                :param [(str)] new_sdk_server: URL of the new SDK server
                    (EXPERIMENTAL) (Optional)
                :param [(bool)] include_purchase_receipt: lookup and include a
                    receipt if this wallet has purchased the claim being
                        resolved (Optional)
                :param [(bool)] include_is_my_output: lookup and include a
                    boolean indicating if claim being resolved is yours
                        (Optional)
                :param [(bool)] include_sent_supports: lookup and sum the
                    total amount of supports you've made to this claim
                        (Optional)
                :param [(bool)] include_sent_tips: lookup and sum the total
                    amount of tips you've made to this claim (only makes sense
                        when claim is not yours) (Optional)
                :param [(bool)] include_received_tips: lookup and sum the
                    total amount of tips you've received to this claim (only
                        makes sense when claim is yours) (Optional)
                :return:
        Dictionary of results, keyed by url
            '<url>': {
                    If a resolution error occurs:
                    'error': Error message

                    If the url resolves to a channel or a claim in a channel:
                    'certificate': {
                        'address': (str) claim address,
                        'amount': (float) claim amount,
                        'effective_amount': (float) claim amount including supports,
                        'claim_id': (str) claim id,
                        'claim_sequence': (int) claim sequence number (or -1 if unknown),
                        'decoded_claim': (bool) whether or not the claim value was decoded,
                        'height': (int) claim height,
                        'confirmations': (int) claim depth,
                        'timestamp': (int) timestamp of the block that included this claim tx,
                        'has_signature': (bool) included if decoded_claim
                        'name': (str) claim name,
                        'permanent_url': (str) permanent url of the certificate claim,
                        'supports: (list) list of supports [{'txid': (str) txid,
                                                             'nout': (int) nout,
                                                             'amount': (float) amount}],
                        'txid': (str) claim txid,
                        'nout': (str) claim nout,
                        'signature_is_valid': (bool), included if has_signature,
                        'value': ClaimDict if decoded, otherwise hex string
                    }

                    If the url resolves to a channel:
                    'claims_in_channel': (int) number of claims in the channel,

                    If the url resolves to a claim:
                    'claim': {
                        'address': (str) claim address,
                        'amount': (float) claim amount,
                        'effective_amount': (float) claim amount including supports,
                        'claim_id': (str) claim id,
                        'claim_sequence': (int) claim sequence number (or -1 if unknown),
                        'decoded_claim': (bool) whether or not the claim value was decoded,
                        'height': (int) claim height,
                        'depth': (int) claim depth,
                        'has_signature': (bool) included if decoded_claim
                        'name': (str) claim name,
                        'permanent_url': (str) permanent url of the claim,
                        'channel_name': (str) channel name if claim is in a channel
                        'supports: (list) list of supports [{'txid': (str) txid,
                                                             'nout': (int) nout,
                                                             'amount': (float) amount}]
                        'txid': (str) claim txid,
                        'nout': (str) claim nout,
                        'signature_is_valid': (bool), included if has_signature,
                        'value': ClaimDict if decoded, otherwise hex string
                    }
            }
        """
        __params_map = {
            "urls": urls,
            "wallet_id": wallet_id,
            "new_sdk_server": new_sdk_server,
            "include_purchase_receipt": include_purchase_receipt,
            "include_is_my_output": include_is_my_output,
            "include_sent_supports": include_sent_supports,
            "include_sent_tips": include_sent_tips,
            "include_received_tips": include_received_tips,
        }

        request = await self.make_request(
            SERVER_ADDRESS, "resolve", __params_map
        )
        return request

    async def routing_table_get(self):
        """Get DHT routing information

                :return:
        (dict) dictionary containing routing and peer information
            {
                "buckets": {
                    <bucket index>: [
                        {
                            "address": (str) peer address,
                            "udp_port": (int) peer udp port,
                            "tcp_port": (int) peer tcp port,
                            "node_id": (str) peer node id,
                        }
                    ]
                },
                "node_id": (str) the local dht node id
            }
        """
        __params_map = {}

        request = await self.make_request(
            SERVER_ADDRESS, "routing_table_get", __params_map
        )
        return request

    async def status(self):
        """Get daemon status

                :return:
        (dict) lbrynet-daemon status
            {
                'installation_id': (str) installation id - base58,
                'is_running': (bool),
                'skipped_components': (list) [names of skipped components (str)],
                'startup_status': { Does not include components which have been skipped
                    'blob_manager': (bool),
                    'blockchain_headers': (bool),
                    'database': (bool),
                    'dht': (bool),
                    'exchange_rate_manager': (bool),
                    'hash_announcer': (bool),
                    'peer_protocol_server': (bool),
                    'file_manager': (bool),
                    'libtorrent_component': (bool),
                    'upnp': (bool),
                    'wallet': (bool),
                },
                'connection_status': {
                    'code': (str) connection status code,
                    'message': (str) connection status message
                },
                'blockchain_headers': {
                    'downloading_headers': (bool),
                    'download_progress': (float) 0-100.0
                },
                'wallet': {
                    'connected': (str) host and port of the connected spv server,
                    'blocks': (int) local blockchain height,
                    'blocks_behind': (int) remote_height - local_height,
                    'best_blockhash': (str) block hash of most recent block,
                    'is_encrypted': (bool),
                    'is_locked': (bool),
                    'connected_servers': (list) [
                        {
                            'host': (str) server hostname,
                            'port': (int) server port,
                            'latency': (int) milliseconds
                        }
                    ],
                },
                'libtorrent_component': {
                    'running': (bool) libtorrent was detected and started successfully,
                },
                'dht': {
                    'node_id': (str) lbry dht node id - hex encoded,
                    'peers_in_routing_table': (int) the number of peers in the routing table,
                },
                'blob_manager': {
                    'finished_blobs': (int) number of finished blobs in the blob manager,
                    'connections': {
                        'incoming_bps': {
                            <source ip and tcp port>: (int) bytes per second received,
                        },
                        'outgoing_bps': {
                            <destination ip and tcp port>: (int) bytes per second sent,
                        },
                        'total_outgoing_mps': (float) megabytes per second sent,
                        'total_incoming_mps': (float) megabytes per second received,
                        'time': (float) timestamp
                    }
                },
                'hash_announcer': {
                    'announce_queue_size': (int) number of blobs currently queued to be announced
                },
                'file_manager': {
                    'managed_files': (int) count of files in the stream manager,
                },
                'upnp': {
                    'aioupnp_version': (str),
                    'redirects': {
                        <TCP | UDP>: (int) external_port,
                    },
                    'gateway': (str) manufacturer and model,
                    'dht_redirect_set': (bool),
                    'peer_redirect_set': (bool),
                    'external_ip': (str) external ip address,
                }
            }
        """
        __params_map = {}

        request = await self.make_request(
            SERVER_ADDRESS, "status", __params_map
        )
        return request

    async def stop(self):
        """Stop lbrynet API server.

                :return:
        (string) Shutdown message
        """
        __params_map = {}

        request = await self.make_request(SERVER_ADDRESS, "stop", __params_map)
        return request

    async def version(self):
        """Get lbrynet API server version information

                :return:
        (dict) Dictionary of lbry version information
            {
                'processor': (str) processor type,
                'python_version': (str) python version,
                'platform': (str) platform string,
                'os_release': (str) os release string,
                'os_system': (str) os name,
                'version': (str) lbrynet version,
                'build': (str) "dev" | "qa" | "rc" | "release",
            }
        """
        __params_map = {}

        request = await self.make_request(
            SERVER_ADDRESS, "version", __params_map
        )
        return request
